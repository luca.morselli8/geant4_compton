# First Example - Compton Scattering Experiment

Simple Compton Scattering experiment simulation using GEANT4 toolkit.

![simulation setup](docs/compton.png)

## Structure

1. Geometry: Aluminum target and NaI scintillator detector (cylinder without 
optical photons simulation)

2. PhysicsList: Custom physics list for "low" energy electromagnetic physics

3. Use of sensitive detector and hit class for recover energy deposited inside the detector volume

4. The user can specify the angle of the detector with respect to the beam direction (511 keV photon) at the start of the program

5. The simulation program writes on a log file the simulation status which is readed by a python monitoring script (testing for future cloud parallelization purpose)

6. Simulated data are saved inside a ROOT file and analyzed with a dedicated macro


![simulated spectra](docs/simulated_spectra.png)
