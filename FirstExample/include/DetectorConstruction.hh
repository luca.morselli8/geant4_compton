#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"
#include "G4LogicalVolume.hh"

class DetectorConstruction : public G4VUserDetectorConstruction
{
  public:

    // Constructor & Destructor
    DetectorConstruction();
    DetectorConstruction(G4double );
    virtual ~DetectorConstruction();

    // Mandatory class
    virtual G4VPhysicalVolume* Construct();
    virtual void ConstructSDandField();

  private:

    void DefineMaterials();
    G4VPhysicalVolume* ConstructDetector();

    // Materials
    G4Material*  fScintillatorMaterial;
    G4Material*  fWorldMaterial;
    G4Material*  fTargetMaterial;

    // Logical volumes
    G4LogicalVolume* fLogicalScintillator;
    G4LogicalVolume* fLogicalTarget;
    G4LogicalVolume* fLogicalWorld;

    // Physical Volumes
    G4VPhysicalVolume* PhysicsWorld;

    // User parameters
    G4double fDetectorAngle;

};


#endif
