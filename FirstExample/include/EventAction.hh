#ifndef EventAction_h
#define EventAction_h 1

#include "G4UserEventAction.hh"
#include "ScintillatorHit.hh"
#include "globals.hh"

#include <fstream>

class EventAction : public G4UserEventAction
{

  public:

    EventAction();
    virtual ~EventAction();

    virtual void BeginOfEventAction(const G4Event* event);
    virtual void EndOfEventAction(const G4Event* event);

  private:

    ScintillatorHitsCollection* GetHitsCollection(G4int hcID, const G4Event* event) const;

    // Data Member
    G4int fScintHCID;

};

#endif
