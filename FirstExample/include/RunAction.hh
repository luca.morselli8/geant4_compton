#ifndef RunAction_h
#define RunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"
#include <string>

class G4Run;

class RunAction : public G4UserRunAction
{

  public:

    RunAction();
    RunAction(G4double angle);
    virtual ~RunAction();

    virtual void BeginOfRunAction(const G4Run*);
    virtual void EndOfRunAction(const G4Run*);

  private:

    G4double fDetectorAngle;
    std::string fOutputAngle;

};

#endif
