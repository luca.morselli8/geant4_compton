#ifndef ScintillatorHit_h
#define ScintillatorHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "G4Threading.hh"


class ScintillatorHit : public G4VHit
{
  public:

    ScintillatorHit();
    ScintillatorHit(const ScintillatorHit&);

    virtual ~ScintillatorHit();

    // Operators

     const ScintillatorHit& operator=(const ScintillatorHit&);
     G4int operator==(const ScintillatorHit&) const;

     inline void * operator new(size_t);
     inline void   operator delete(void*);

     // Methods from base class
     virtual void Draw(){}
     virtual void Print();

     // Methods to handle data
     void Add(G4double de);

     // get methods
     G4double GetEdep() const;

  private:

    G4double fEdep;

};

using ScintillatorHitsCollection = G4THitsCollection<ScintillatorHit>;

extern G4ThreadLocal G4Allocator<ScintillatorHit>* ScintillatorHitAllocator;

inline void* ScintillatorHit::operator new(size_t)
{
  if(!ScintillatorHitAllocator)
  {
    ScintillatorHitAllocator = new G4Allocator<ScintillatorHit>;
  }

  void *hit;

  hit = (void*) ScintillatorHitAllocator->MallocSingle();

  return hit;
}

inline void ScintillatorHit::operator delete(void *hit)
{
  if(!ScintillatorHitAllocator)
  {
    ScintillatorHitAllocator = new G4Allocator<ScintillatorHit>;
  }

  ScintillatorHitAllocator->FreeSingle((ScintillatorHit*)hit);
}

inline void ScintillatorHit::Add(G4double de)
{
  fEdep += de;
}

inline G4double ScintillatorHit::GetEdep() const { return fEdep; }


#endif
