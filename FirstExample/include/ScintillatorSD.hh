#ifndef ScintillatorSD_h
#define ScintillatorSD_h 1

#include "G4VSensitiveDetector.hh"
#include "ScintillatorHit.hh"

#include <vector>

class G4Step;
class G4HCofThisEvent;

class ScintillatorSD : public G4VSensitiveDetector
{
  public:

    ScintillatorSD(const G4String& name,
                   const G4String& hitsCollectionName);
    virtual ~ScintillatorSD();

    // Methods from base class
    virtual void   Initialize(G4HCofThisEvent* hitCollection);
    virtual G4bool ProcessHits(G4Step* step, G4TouchableHistory* history);
    virtual void   EndOfEvent(G4HCofThisEvent* hitCollection);

  private:

    ScintillatorHitsCollection* fHitsCollection;

};

#endif
