/*
*
*
*
*
*/

TH1F * get_histogram_from_file(std::string filename);
void normalize_histogram(TH1F* h);

void analysis()
{
  // Fill histograms
  TH1F * h30 = get_histogram_from_file("SimulationResults/simulated_data_30.root");
  TH1F * h40 = get_histogram_from_file("SimulationResults/simulated_data_40.root");
  TH1F * h50 = get_histogram_from_file("SimulationResults/simulated_data_50.root");
  TH1F * h60 = get_histogram_from_file("SimulationResults/simulated_data_60.root");
  TH1F * h70 = get_histogram_from_file("SimulationResults/simulated_data_70.root");
  TH1F * h80 = get_histogram_from_file("SimulationResults/simulated_data_80.root");
  TH1F * h90 = get_histogram_from_file("SimulationResults/simulated_data_90.root");

  // Normalizing all histograms
  normalize_histogram(h30);
  normalize_histogram(h40);
  normalize_histogram(h50);
  normalize_histogram(h60);
  normalize_histogram(h70);
  normalize_histogram(h80);
  normalize_histogram(h90);

  // Plot histograms
  h30->SetStats(kFALSE);
  h30->SetLineColor(kRed+1);
  h30->GetXaxis()->SetTitle("Energy [keV]");
  h30->GetYaxis()->SetTitle("Normalized counts [#]");
  h30->GetYaxis()->SetRangeUser(0,0.09);
  h30->Draw("HIST");

  h40->SetLineColor(kRed+2);
  h40->Draw("HIST SAME");  

  h50->SetLineColor(kBlue+1);
  h50->Draw("HIST SAME");

  h60->SetLineColor(kBlue+2);
  h60->Draw("HIST SAME");  

  h70->SetLineColor(kOrange+1);
  h70->Draw("HIST SAME");

  h80->SetLineColor(kOrange+2);
  h80->Draw("HIST SAME");  

  h90->SetLineColor(kGreen+1);
  h90->Draw("HIST SAME"); 

  // Setting up legend
  TLegend *legend = new TLegend(0.1,0.7,0.48,0.9);
  legend->SetHeader("Compton Scattering Simulation","C");
  legend->AddEntry(h30,"30^{#circ} Degrees","l");
  legend->AddEntry(h40,"40^{#circ} Degrees","l");
  legend->AddEntry(h50,"50^{#circ} Degrees","l");
  legend->AddEntry(h60,"60^{#circ} Degrees","l");
  legend->AddEntry(h70,"70^{#circ} Degrees","l");
  legend->AddEntry(h80,"80^{#circ} Degrees","l");
  legend->AddEntry(h90,"90^{#circ} Degrees","l");

  legend->Draw("SAME");

  return ;
}

void normalize_histogram(TH1F * h)
{
  Double_t scale = 1.0/(h->Integral());
  h->Scale(scale);

  return ;
}

TH1F * get_histogram_from_file(std::string filename)
{
  // Print to std::out
  std::size_t pos = filename.find(".root");
  std::cout << "Reading from " + filename.substr(pos-2,2) + " file" << std::endl;

  // Retrieve ROOT file
  TFile * infile = new TFile(filename.c_str());
  // Get the tree
  TTree * tree = (TTree*) infile->Get("tree");

  std::string branch_name = filename.substr(pos-2,2)+"_degrees";

  double energy;
  TBranch * branch = (TBranch*) tree->GetBranch(branch_name.c_str());
  branch->SetAddress(&energy);

  TH1F *h = new TH1F(branch_name.c_str(), "",300,0,600);

  for(size_t i = 0; i < branch->GetEntries(); i++)
  { 
    branch->GetEntry(i);
    // Simulated energy value is in MeV
    // While histogram are setted in keV
    energy = energy*1000;
    
    if( (energy)>0.1 )
    {
      h->Fill(energy);
    }
  }

  return h;
}
