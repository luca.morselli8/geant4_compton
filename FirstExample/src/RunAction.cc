#include "RunAction.hh"
#include "Analysis.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

#include <string>

RunAction::RunAction()
: G4UserRunAction(), fDetectorAngle(0.)
{}

RunAction::RunAction(G4double angle)
: G4UserRunAction(), fDetectorAngle(angle)
{

  // Set printing event number per each event
  G4RunManager::GetRunManager()->SetPrintProgress(10000);

  auto analysisManger = G4AnalysisManager::Instance();
  G4cout << "Using " << analysisManger->GetType() << G4endl;

  analysisManger->SetVerboseLevel(1);
  analysisManger->SetNtupleMerging(true);

  // Creating Ntuple
  analysisManger->CreateNtuple("tree","Compton Scattering Experiment");

  std::string str = std::to_string(fDetectorAngle);
  str.erase ( str.find_last_not_of('0') + 1 , std::string::npos );

  if(str.back() == '.')
    str = str.substr(0, str.size()-1);

  fOutputAngle = str;

  analysisManger->CreateNtupleDColumn(fOutputAngle+"_degrees");
  analysisManger->FinishNtuple();
}

RunAction::~RunAction()
{
  delete G4AnalysisManager::Instance();
}

void RunAction::BeginOfRunAction(const G4Run* )
{
  // Get Analysis Manager
  auto analysisManger = G4AnalysisManager::Instance();

  // Open an output file

  G4String fileName = "SimulationResults/simulated_data_"+fOutputAngle+".root";
  std::cout << fileName << std::endl;
  analysisManger->OpenFile(fileName);

  return ;
}

void RunAction::EndOfRunAction(const G4Run* )
{
  auto analysisManager = G4AnalysisManager::Instance();

  // save histograms & ntuple
  //
  analysisManager->Write();
  analysisManager->CloseFile();

  return ;
}
