#include "EventAction.hh"
#include "ScintillatorSD.hh"
#include "ScintillatorHit.hh"
#include "Analysis.hh"

#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4SDManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4UnitsTable.hh"

#include "Randomize.hh"
#include <iomanip>
#include <fstream>


EventAction::EventAction()
: G4UserEventAction(), fScintHCID(-1)
{
}

EventAction::~EventAction()
{}


ScintillatorHitsCollection* EventAction::GetHitsCollection(G4int hcID, const G4Event* event) const
{

  auto hitsCollection = static_cast<ScintillatorHitsCollection*>
    ( event->GetHCofThisEvent()->GetHC(hcID));

  if ( ! hitsCollection )
  {
    G4ExceptionDescription msg;
    msg << "Cannot access hitsCollection ID " << hcID;
    G4Exception("B4cEventAction::GetHitsCollection()",
    "MyCode0003", FatalException, msg);
  }

  return hitsCollection;

}

void EventAction::BeginOfEventAction(const G4Event* /*event*/)
{
  return ;
}

void EventAction::EndOfEventAction(const G4Event* event)
{
  // Get hits collections IDs (only once)

  if(fScintHCID == -1)
  {
    fScintHCID =
      G4SDManager::GetSDMpointer()->GetCollectionID("ScintillatorHitsCollection");
  }

  auto scintHC = GetHitsCollection(fScintHCID, event);

  auto scintHit  = (*scintHC)[0];

  auto eventID = event->GetEventID();
  auto printModulo = G4RunManager::GetRunManager()->GetPrintProgress();

  

  if ( ( printModulo > 0 ) && ( eventID % printModulo == 0 ) )
  {
    // Stream output file
    std::ofstream fOutputFile("log.txt", std::ofstream::out);
    fOutputFile << eventID << std::endl;
    // Close output file
    fOutputFile.close();
  }

 
  

  // get analysis manager
  auto analysisManager = G4AnalysisManager::Instance();


  // Fill ntuple
  analysisManager->FillNtupleDColumn(0,scintHit->GetEdep());
  analysisManager->AddNtupleRow();

  

  return ;
}
