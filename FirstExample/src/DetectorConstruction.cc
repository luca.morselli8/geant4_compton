#include "DetectorConstruction.hh"
#include "ScintillatorSD.hh"
#include "G4SDManager.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"


DetectorConstruction::DetectorConstruction()
: G4VUserDetectorConstruction(),
fScintillatorMaterial(NULL), fWorldMaterial(NULL), fTargetMaterial(NULL),
fLogicalScintillator(NULL), fLogicalTarget(NULL), fLogicalWorld(NULL),
PhysicsWorld(NULL), fDetectorAngle(0.)
{ }

DetectorConstruction::DetectorConstruction(G4double angle)
: G4VUserDetectorConstruction(),
fScintillatorMaterial(NULL), fWorldMaterial(NULL), fTargetMaterial(NULL),
fLogicalScintillator(NULL), fLogicalTarget(NULL), fLogicalWorld(NULL),
PhysicsWorld(NULL), fDetectorAngle(angle)
{ }

DetectorConstruction::~DetectorConstruction()
{ }

void DetectorConstruction::DefineMaterials()
{
  // Material Database
  G4NistManager* nistManager = G4NistManager::Instance();

  // Setting up air
  fWorldMaterial = nistManager->FindOrBuildMaterial("G4_AIR");

  // Setting up target material
  fTargetMaterial = nistManager->FindOrBuildMaterial("G4_Al");

  // Setting up scintillator material
  fScintillatorMaterial = nistManager->FindOrBuildMaterial("G4_SODIUM_IODIDE");


  return ;

}

G4VPhysicalVolume* DetectorConstruction::ConstructDetector()
{

  G4double worldLenght = 3*m;

  // Define needed solid
  G4Box*  fWorldSolid = new G4Box("world", worldLenght/2,worldLenght/2,worldLenght/2);
  G4Tubs* fTargetSolid = new G4Tubs("target", 0.*cm, 2.*cm, 2*mm, 0.*deg, 360.*deg);
  G4Tubs* fScintillatorSolid = new G4Tubs("scintillator",0.*cm,3.75*cm,3.75*cm,0.*deg, 360.*deg);
  // Assembly logical volumes

  fLogicalWorld = new G4LogicalVolume(fWorldSolid, fWorldMaterial, "World",0,0,0);
  fLogicalTarget = new G4LogicalVolume(fTargetSolid, fTargetMaterial, "Target",0,0,0);
  fLogicalScintillator = new G4LogicalVolume(fScintillatorSolid, fScintillatorMaterial,"Scintillator",0,0,0);

  G4bool fCheckOverlaps = true;

  // World Volume
  PhysicsWorld = new G4PVPlacement(0,               // no rotation
                                   G4ThreeVector(), // at (0,0,0)
                                   fLogicalWorld,         // its logical volume
                                   "World",         // its name
                                   0,               // its mother  volume
                                   false,           // no boolean operations
                                   0,               // copy number
                                   fCheckOverlaps); // checking overlaps

  // Target placement
  new G4PVPlacement(0,
                    G4ThreeVector(0.0*cm,0.0*cm,0.*cm),
                    fLogicalTarget,
                    "Target",
                    fLogicalWorld,
                    false,
                    0,
                    fCheckOverlaps);




  // Detector placed @ 40cm from the target with a user define angle
  G4RotationMatrix * rotationMatrix  = new G4RotationMatrix();
  rotationMatrix->rotateY(-fDetectorAngle*deg);

  new G4PVPlacement(rotationMatrix,
                    G4ThreeVector((40*std::sin(fDetectorAngle*M_PI/180.0))*cm,0.*cm,(40*std::cos(fDetectorAngle*M_PI/180.0))*cm),
                    fLogicalScintillator,
                    "Scintillator",
                    fLogicalWorld,
                    false,
                    0,
                    fCheckOverlaps);

  return PhysicsWorld;
}


G4VPhysicalVolume* DetectorConstruction::Construct()
{
  // Define Materials
  DefineMaterials();

  return ConstructDetector();
}


void DetectorConstruction::ConstructSDandField()
{
  auto scintSD =
    new ScintillatorSD("ScintillatorSD", "ScintillatorHitsCollection");

  G4SDManager::GetSDMpointer()->AddNewDetector(scintSD);
  SetSensitiveDetector("Scintillator", scintSD);

  return ;
}
