#include "ActionInitialization.hh"
#include "PrimaryGeneratorAction.hh"
#include "RunAction.hh"
#include "EventAction.hh"

ActionInitialization::ActionInitialization(){}

ActionInitialization::ActionInitialization(G4double angle)
: G4VUserActionInitialization(),fDetectorAngle(angle)
{}

ActionInitialization::~ActionInitialization(){}

void ActionInitialization::BuildForMaster() const
{
  SetUserAction(new RunAction());
}
void ActionInitialization::Build() const
{
  SetUserAction(new PrimaryGeneratorAction);
  SetUserAction(new RunAction(fDetectorAngle));
  SetUserAction(new EventAction);
}
