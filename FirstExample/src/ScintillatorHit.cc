#include "ScintillatorHit.hh"
#include "G4UnitsTable.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"
#include <iomanip>


G4ThreadLocal G4Allocator<ScintillatorHit>* ScintillatorHitAllocator = 0;

ScintillatorHit::ScintillatorHit()
: G4VHit(), fEdep(0.)
{}

ScintillatorHit::~ScintillatorHit(){}

ScintillatorHit::ScintillatorHit(const ScintillatorHit& right)
: G4VHit()
{
  fEdep = right.fEdep;
}

const ScintillatorHit& ScintillatorHit::operator=(const ScintillatorHit& right)
{
  fEdep = right.fEdep;
  return *this;
}

G4int ScintillatorHit::operator==(const ScintillatorHit& right) const
{
  return ( this == &right ) ? 1 : 0;
}

void ScintillatorHit::Print()
{
  G4cout << "Edep: "
         << std::setw(7) << G4BestUnit(fEdep, "Energy")
         << G4endl;

}
