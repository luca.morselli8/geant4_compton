#include "PhysicsList.hh"

#include "G4DecayPhysics.hh"
#include "G4EmLivermorePhysics.hh"

PhysicsList::PhysicsList()
: G4VUserPhysicsList()
{
  SetVerboseLevel(1);

  RegisterPhysics(new G4DecayPhysics());
  RegisterPhysics(new G4EmLivermorePhysics());

}

PhysicsList::~PhysicsList()
{}

void PhysicsList::SetCuts()
{ return ; }
