#include "ScintillatorSD.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"

ScintillatorSD::ScintillatorSD(
  const G4String& name,
  const G4String& hitsCollectionName)
:
G4VSensitiveDetector(name),
fHitsCollection(nullptr)
{
  collectionName.insert(hitsCollectionName);
}

ScintillatorSD::~ScintillatorSD(){}

void ScintillatorSD::Initialize(G4HCofThisEvent* hce)
{

  // Create hits collection

  fHitsCollection =
    new ScintillatorHitsCollection(SensitiveDetectorName, collectionName[0]);

  auto hcID =
    G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);

  hce->AddHitsCollection(hcID, fHitsCollection);

  // Creating one hit
  fHitsCollection->insert(new ScintillatorHit());

}

G4bool ScintillatorSD::ProcessHits(G4Step* step, G4TouchableHistory*)
{
  // custom code per hit processing
  // Get energy deposition in each detector
  auto edep = step->GetTotalEnergyDeposit();

  if(edep == 0.) return false;

  // Get Scintillator ID

  auto hit = (*fHitsCollection)[0];

  // Exception Handling --> Approfondire
  if( !hit )
  {
    G4ExceptionDescription msg;
    msg << "Cannot access hit 0";
    G4Exception("ScintillatorSD::ProcessHits()","Error 1", FatalException,msg);
  }

  hit->Add(edep);

  return true;

}

void ScintillatorSD::EndOfEvent(G4HCofThisEvent*)
{ return ; }
